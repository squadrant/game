﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Jitter
{
    internal class Player : Sprite
    {
        public static Texture2D playerSprite;
        public static Texture2D playerYellowSprite;
        public static Texture2D playerGreenSprite;
        public static Texture2D playerBlueSprite;
        public static Texture2D playerRedSprite;
        public static Texture2D playerOrangeSprite;

        public static Player main;

        public static Vector2 initialPosition = new Vector2(MainGame.tileSize, MainGame.screenSize.Y - MainGame.tileSize * 2);

        private readonly float maxSpeed = 15;
        private readonly float maxVerticalSpeed = 20;

        public bool isGrounded = false;

        private Tile.Type playerColor;

        public Player()
        {
            main = this;
            position = Player.initialPosition;
            velocity = Vector2.Zero;
            base.sprite = playerSprite;
        }

        public void AddForce(Vector2 force)
        {
            velocity += force;

            if (velocity.X > maxSpeed)
            {
                velocity.X = maxSpeed;
            }

            if (velocity.X < -maxSpeed)
            {
                velocity.X = -maxSpeed;
            }

            if (velocity.Y > maxVerticalSpeed)
            {
                velocity.Y = maxVerticalSpeed;
            }

            if (velocity.Y < -maxVerticalSpeed)
            {
                velocity.Y = -maxVerticalSpeed;
            }
        }

        public Vector2 GetSpeed()
        {
            return velocity;
        }


        // Called every frame
        public void Update(List<Tile> tiles, List<Door> doors)
        {
            if (velocity != Vector2.Zero)
            {
                velocity.X -= velocity.X / 6f;

                if (velocity.X <= 0.1f && velocity.X >= -0.1f)
                {
                    velocity.X = 0;
                }

                if (velocity.Y <= 0.1f && velocity.Y >= -0.1f)
                {
                    velocity.Y = 0;
                }
            }

            if (Player.main.position.Y > 3000)
            {
                Player.main.velocity = Vector2.Zero;
                Player.main.position = Player.initialPosition;
            }

            velocity.Y += 1.5f;

            foreach (Tile t in tiles)
            {
                if (velocity.Y > 0 && IsTouchingTop(t))
                {
                    velocity.Y = 0;
                    position.Y = t.position.Y - t.sprite.Height;
                    CheckColor(t);
                    isGrounded = true;
                }

                if (velocity.Y < 0 && IsTouchingBottom(t))
                {
                    velocity.Y = 0;
                    position.Y = t.position.Y + t.sprite.Height;
                    CheckColor(t);
                }

                if (velocity.X > 0 && IsTouchingLeft(t))
                {
                    velocity.X = 0;
                    position.X = t.position.X - t.sprite.Width;
                    CheckColor(t);
                }

                if (velocity.X < 0 && IsTouchingRight(t))
                {
                    velocity.X = 0;
                    position.X = t.position.X + t.sprite.Width;
                    CheckColor(t);
                }
            }
            if (doors.Count > 0)
            {
                foreach (Door d in doors)
                {
                    // WILL BREAK AFTER IMPLEMENTING MORE TYPES
                    /*
                    if (IsTouchingTop(d) || IsTouchingBottom(d) || IsTouchingLeft(d) || IsTouchingRight(d))
                    {
                        if (d.doorColor.ToString() == playerColor.ToString())
                        {
                            LevelEditor.main.LoadLevel(d.levelToLoad);
                            break;
                        }
                    }
                    */
                    if (IsTouchingLeft(d))
                    {
                        bool foundOther = false;
                        foreach (Door otherD in doors)
                        {
                            if (otherD.position != d.position && otherD.doorColor == d.doorColor)
                            {
                                if (d.doorColor.ToString() == playerColor.ToString())
                                {
                                    position = otherD.position;
                                    position.X += (MainGame.tileSize + 2);
                                    foundOther = true;
                                    break;
                                }
                            }
                        }
                        if (!foundOther)
                        {
                            if (d.doorColor.ToString() == playerColor.ToString() && playerColor != Tile.Type.Gray)
                            {
                                LevelManager.LoadLevel(d.levelToLoad);
                                return;
                            }
                        }
                    }

                    if (IsTouchingRight(d))
                    {
                        bool foundOther = false;
                        foreach (Door otherD in doors)
                        {
                            if (otherD.position != d.position && otherD.doorColor == d.doorColor)
                            {
                                if (d.doorColor.ToString() == playerColor.ToString())
                                {
                                    position = otherD.position;
                                    position.X -= (MainGame.tileSize + 2);
                                    foundOther = true;
                                    break;
                                }
                            }
                        }
                        if (!foundOther)
                        {
                            if (d.doorColor.ToString() == playerColor.ToString() && playerColor != Tile.Type.Gray)
                            {
                                LevelManager.LoadLevel(d.levelToLoad);
                                return;
                            }
                        }
                    }
                }
            }

            // Main movement. Smoothed with deltaTime
            position += velocity * MainGame.instance.deltaTime * 60;
        }

        private void CheckColor(Tile t)
        {
            if (t.tileColor != Tile.Type.Gray)
            {
                playerColor = t.tileColor;

                if (playerColor == Tile.Type.Gray)
                {
                    //base.sprite = Tile.tileSprite;
                }
                else if (playerColor == Tile.Type.Yellow)
                {
                    base.sprite = playerYellowSprite;
                }
                else if (playerColor == Tile.Type.Green)
                {
                    base.sprite = playerGreenSprite;
                }
                else if (playerColor == Tile.Type.Blue)
                {
                    base.sprite = playerBlueSprite;
                }
                else if (playerColor == Tile.Type.Red)
                {
                    base.sprite = playerRedSprite;
                }
                else if (playerColor == Tile.Type.Orange)
                {
                    base.sprite = playerOrangeSprite;
                }
            }
        }

    }
}
