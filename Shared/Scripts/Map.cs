﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Jitter
{
    internal class Map
    {
        public static Map map;

        private List<Tile> tiles = new List<Tile>();
        private List<Door> doors = new List<Door>();

        public void LoadMap()
        {
            int tileSize = MainGame.tileSize;
            int screenWidth = (int)MainGame.screenSize.X;
            int screenHeight = (int)MainGame.screenSize.Y;

            map = this;

            //AddFrame();

            tiles.Add(new Tile(new Vector2(MainGame.tileSize, 960), Tile.Type.Gray));

            //doors.Add(new Door(new Vector2(1792, 960), Door.Colors.Blue));
            //doors.Add(new Door(new Vector2(Game1.tileSize, 960), Door.Colors.Green));
        }

        public void AddFrame()
        {
            for (int i = 0; i < MainGame.screenSize.X; i += MainGame.tileSize)
            {
                tiles.Add(new Tile(new Vector2(i, 0), Tile.Type.Gray));
                tiles.Add(new Tile(new Vector2(i, 16 * MainGame.tileSize), Tile.Type.Gray));
            }

            for (int i = 0; i < MainGame.screenSize.Y; i += MainGame.tileSize)
            {
                tiles.Add(new Tile(new Vector2(0, i), Tile.Type.Gray));
                tiles.Add(new Tile(new Vector2(MainGame.screenSize.X - MainGame.tileSize, i), Tile.Type.Gray));
            }
        }

        public void AddTile(Vector2 position, Tile.Type color)
        {
            tiles.Add(new Tile(position, color));
        }

        public void AddDoor(Vector2 position, Door.Colors color, string level)
        {
            doors.Add(new Door(position, color, level));
        }

        public void DeleteTile(int index)
        {
            tiles.RemoveAt(index);
        }

        public List<Tile> GetTiles()
        {
            return tiles;
        }

        public List<Door> GetDoors()
        {
            return doors;
        }

        // Called every frame
        public void DrawMap(SpriteBatch spriteBatch)
        {
            foreach (Tile t in tiles)
            {
                t.Draw(spriteBatch);
            }

            foreach (Door d in doors)
            {
                d.Draw(spriteBatch);
            }
        }

        public void ClearTiles()
        {
            tiles.Clear();
            doors.Clear();
        }
    }
}
