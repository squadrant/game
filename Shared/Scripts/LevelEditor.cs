﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;

namespace Jitter
{
    internal class LevelEditor
    {
        public static bool enabled = false;

        public static LevelEditor main;

        public List<Tile> grid = new List<Tile>();

        public Tile mouse;
        private MouseState lastMouseState;
        private MouseState currentMouseState;

        public LevelEditor()
        {
            LevelEditor.main = this;

            lastMouseState = Mouse.GetState();
            currentMouseState = Mouse.GetState();

            mouse = new Tile(Mouse.GetState().Position.ToVector2(), Tile.Type.Gray);

            int scale = 5;

            for (int x = -(int)MainGame.screenSize.X * scale; x < MainGame.screenSize.X * scale; x += MainGame.tileSize)
            {
                for (int y = -(int)MainGame.screenSize.X * scale; y < MainGame.screenSize.Y * scale; y += MainGame.tileSize)
                {
                    grid.Add(new Tile(new Vector2(x, y), Tile.Type.Construction));
                }
            }

        }

        public void Update()
        {
            if (!enabled)
            {
                return;
            }

            // The active state from the last frame is now old
            lastMouseState = currentMouseState;

            // Get the mouse state relevant for this frame
            currentMouseState = Mouse.GetState();

            //Console.WriteLine(Mouse.GetState().Position.X + " " + Mouse.GetState().Position.Y);

            mouse.position = SnapToGrid(new Vector2(Mouse.GetState().Position.X - MainGame.tileSize / 2, Mouse.GetState().Position.Y - MainGame.tileSize / 2));

            // Recognize a single click of the left mouse button
            if (lastMouseState.LeftButton == ButtonState.Released && currentMouseState.LeftButton == ButtonState.Pressed)
            {
                PlaceTile(mouse.position);
            }

            if (lastMouseState.MiddleButton == ButtonState.Released && currentMouseState.MiddleButton == ButtonState.Pressed)
            {
                //Camera.main.Zoom = 1f;
            }

            if (lastMouseState.RightButton == ButtonState.Released && currentMouseState.RightButton == ButtonState.Pressed)
            {

                mouse.tileColor = (Tile.Type)Enum.ToObject(typeof(Tile.Type), (int)mouse.tileColor + 1);


                if ((int)mouse.tileColor >= Enum.GetNames(typeof(Tile.Type)).Length - 1)
                {
                    //Console.WriteLine((int)mouse.tileColor + "  " + (Enum.GetNames(typeof(Tile.Type)).Length - 1));
                    mouse.tileColor = Tile.Type.Gray;
                }

                mouse.UpdateColor();
            }
        }

        public void PlaceTile(Vector2 position)
        {
            foreach (Tile t in Map.map.GetTiles())
            {
                if (t.position == position)
                {
                    if (mouse.tileColor == Tile.Type.Delete)
                    {
                        RemoveTile(position);
                        return;
                    }

                    if (mouse.tileColor != Tile.Type.Construction && mouse.tileColor != Tile.Type.Delete)
                    {
                        t.tileColor = mouse.tileColor;
                        t.UpdateColor();
                        return;
                    }
                }
            }

            if (mouse.tileColor != Tile.Type.Construction && mouse.tileColor != Tile.Type.Delete)
            {
                Map.map.AddTile(position, mouse.tileColor);
            }
        }

        public void RemoveTile(Vector2 position)
        {
            for (int i = 0; i < Map.map.GetTiles().Count; i++)
            {
                if (Map.map.GetTiles()[i].position == position)
                {
                    Map.map.DeleteTile(i);
                    return;
                }
            }
        }

        // DO NOT TOUCH THE FLOOR FUNCTION
        public Vector2 SnapToGrid(Vector2 position)
        {
            position.X -= MainGame.screenSize.X / 2;
            position.Y -= MainGame.screenSize.Y / 2;
            position += Camera.main.Position;


            if (position.X % MainGame.tileSize != 0)
            {
                position.X = (float)Math.Floor(position.X - position.X % MainGame.tileSize);
            }

            if (position.Y % MainGame.tileSize != 0)
            {
                position.Y = (float)Math.Floor(position.Y - position.Y % MainGame.tileSize);
            }

            // Adding the god damn offset
            //position.Y += 28;

            return position;
        }
    }
}
