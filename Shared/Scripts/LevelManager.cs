﻿using Microsoft.Xna.Framework;
using System;
using System.IO;

namespace Jitter
{
    internal class LevelManager
    {

        /* Manage the levels in here. All the logic goes here */

        public static void StartGame()
        {
            LoadLevel("level-01");
        }

        public static void SaveLevel(String fileName)
        {
            using (TextWriter tw = new StreamWriter(fileName + ".txt"))
            {
                foreach (Tile t in Map.map.GetTiles())
                {
                    tw.WriteLine(t.ToString());
                }

                foreach (Door d in Map.map.GetDoors())
                {
                    tw.WriteLine(d.ToString());
                }
            }
        }

        public static void LoadLevel(String fileName)
        {
            if (!File.Exists(fileName + ".txt"))
            {
                return;
            }

            Map.map.ClearTiles();

            string[] data;
            foreach (string line in File.ReadLines(fileName + ".txt"))
            {
                data = line.Split('|');

                if (data[0] == "D")
                {
                    Map.map.AddDoor(new Vector2(Int64.Parse(data[1]), Int64.Parse(data[2])), (Door.Colors)Enum.ToObject(typeof(Door.Colors), Int64.Parse(data[3])), data[4]);
                    continue;
                }

                Map.map.AddTile(new Vector2(Int64.Parse(data[0]), Int64.Parse(data[1])), (Tile.Type)Enum.ToObject(typeof(Tile.Type), Int64.Parse(data[2])));
            }

            foreach (Door d in Map.map.GetDoors())
            {
                if (d.doorColor == Door.Colors.Gray)
                {
                    Player.initialPosition = d.position;
                    break;
                }
            }

            Player.main.position = Player.initialPosition;

            if (Camera.main != null)
            {
                Camera.main.Position = Player.main.position;
            }
        }
    }
}
