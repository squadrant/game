﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Jitter
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class MainGame : Game
    {
        public static Vector2 screenSize = new Vector2(1920, 1080);
        public static int tileSize = 64;

        public float deltaTime = 0;

        public static MainGame instance;

        private readonly bool isDebug = true;

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch, uiBatch;
        private SpriteFont debugFont;
        private Player player;

        private Map map;

        public MainGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            MainGame.instance = this;
            // TODO: Add your initialization logic here

            this.Window.Title = "Jitter";

            //this.IsMouseVisible = true;

            Vector2 debugScreenSize = new Vector2(graphics.GraphicsDevice.Adapter.CurrentDisplayMode.Width, graphics.GraphicsDevice.Adapter.CurrentDisplayMode.Height);

            debugScreenSize *= 0.85f;

            graphics.PreferredBackBufferWidth = (int)screenSize.X;
            graphics.PreferredBackBufferHeight = (int)screenSize.Y;

            if (!isDebug)
            {
                graphics.ToggleFullScreen();
            }
            else
            {
                Window.IsBorderless = true;
                graphics.PreferredBackBufferWidth = (int)debugScreenSize.X;
                graphics.PreferredBackBufferHeight = (int)debugScreenSize.Y;
                this.Window.Position = new Point((graphics.GraphicsDevice.Adapter.CurrentDisplayMode.Width - (int)debugScreenSize.X) / 2, (graphics.GraphicsDevice.Adapter.CurrentDisplayMode.Height - (int)debugScreenSize.Y) / 2);
            }

            screenSize = new Vector2(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);

            graphics.ApplyChanges();

            base.Initialize();

            Camera.main = new Camera(GraphicsDevice.Viewport);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            uiBatch = new SpriteBatch(GraphicsDevice);

            // Player
            string playerDirectory = "Graphics/Player/";
            Player.playerSprite = this.Content.Load<Texture2D>(playerDirectory + "player");
            Player.playerYellowSprite = this.Content.Load<Texture2D>(playerDirectory + "player-yellow");
            Player.playerGreenSprite = this.Content.Load<Texture2D>(playerDirectory + "player-green");
            Player.playerBlueSprite = this.Content.Load<Texture2D>(playerDirectory + "player-blue");
            Player.playerRedSprite = this.Content.Load<Texture2D>(playerDirectory + "player-red");
            Player.playerOrangeSprite = this.Content.Load<Texture2D>(playerDirectory + "player-orange");

            // Tiles
            string tileDirectory = "Graphics/Tiles/";
            Tile.tileSprite = this.Content.Load<Texture2D>(tileDirectory + "tile-wall");
            Tile.tileYellowSprite = this.Content.Load<Texture2D>(tileDirectory + "tile-wall-yellow");
            Tile.tileGreenSprite = this.Content.Load<Texture2D>(tileDirectory + "tile-wall-green");
            Tile.tileBlueSprite = this.Content.Load<Texture2D>(tileDirectory + "tile-wall-blue");
            Tile.tileRedSprite = this.Content.Load<Texture2D>(tileDirectory + "tile-wall-red");
            Tile.tileOrangeSprite = this.Content.Load<Texture2D>(tileDirectory + "tile-wall-orange");

            // Other Tiles
            Tile.constructionTileSprite = this.Content.Load<Texture2D>(tileDirectory + "grid-tile");
            Tile.deleteTileSprite = this.Content.Load<Texture2D>(tileDirectory + "delete-tile");

            // Door Tiles
            string doorDirectory = "Graphics/Door/";
            Door.doorSprite = this.Content.Load<Texture2D>(doorDirectory + "tile-door");
            Door.doorYellowSprite = this.Content.Load<Texture2D>(doorDirectory + "tile-door-yellow");
            Door.doorGreenSprite = this.Content.Load<Texture2D>(doorDirectory + "tile-door-green");
            Door.doorBlueSprite = this.Content.Load<Texture2D>(doorDirectory + "tile-door-blue");
            Door.doorRedSprite = this.Content.Load<Texture2D>(doorDirectory + "tile-door-red");
            Door.doorOrangeSprite = this.Content.Load<Texture2D>(doorDirectory + "tile-door-orange");

            debugFont = Content.Load<SpriteFont>("Fonts/DebugFont");

            player = new Player();

            map = new Map();
            map.LoadMap();

            LevelEditor.main = new LevelEditor();
            //levelEditor.LoadLevel("level-01");

            LevelManager.StartGame();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            Content.Unload();
            Content.Dispose();
        }

        private float timer = 0f;
        private readonly float maxTimerTime = 1f;
        private bool canPress = true;

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                Exit();
            }

            // Poll for current keyboard state
            KeyboardState state = Keyboard.GetState();

            // Move our character based on keys being pressed
            if (state.IsKeyDown(Keys.D))
            {
                player.AddForce(new Vector2(2, 0));
            }

            if (state.IsKeyDown(Keys.A))
            {
                player.AddForce(new Vector2(-2, 0));
            }

            // In Update, or some code called every frame:
            var gamePadState = GamePad.GetState(PlayerIndex.One);
            // Use gamePadState to move the character
            player.AddForce(new Vector2(gamePadState.ThumbSticks.Left.X * 2, gamePadState.ThumbSticks.Left.Y * 2));

            // Level editor
            if (state.IsKeyDown(Keys.F1) && canPress)
            {
                LevelEditor.enabled = !LevelEditor.enabled;
                canPress = false;
            }

            if (state.IsKeyDown(Keys.R) && canPress)
            {
                player.position = Player.initialPosition;
                player.velocity.Y = 0;
                canPress = false;
            }

            if (state.IsKeyDown(Keys.D1) && canPress && state.IsKeyDown(Keys.LeftControl))
            {
                LevelManager.SaveLevel("level-01");
                canPress = false;
            }
            else if (state.IsKeyDown(Keys.D1) && canPress)
            {
                LevelManager.LoadLevel("level-01");
                canPress = false;
            }

            if (state.IsKeyDown(Keys.D2) && canPress && state.IsKeyDown(Keys.LeftControl))
            {
                LevelManager.SaveLevel("level-02");
                canPress = false;
            }
            else if (state.IsKeyDown(Keys.D2) && canPress)
            {
                LevelManager.LoadLevel("level-02");
                canPress = false;
            }

            if (state.IsKeyDown(Keys.D3) && canPress && state.IsKeyDown(Keys.LeftControl))
            {
                LevelManager.SaveLevel("level-03");
                canPress = false;
            }
            else if (state.IsKeyDown(Keys.D3) && canPress)
            {
                LevelManager.LoadLevel("level-03");
                canPress = false;
            }

            // Checking when we can press again
            if (!canPress)
            {
                timer += deltaTime;

                if (timer > maxTimerTime)
                {
                    canPress = true;
                    timer = 0;
                }
            }

            // Jump
            if ((state.IsKeyDown(Keys.Space) || (gamePadState.IsButtonDown(Buttons.A))) && player.isGrounded)
            {
                player.AddForce(new Vector2(0, -40));
                player.isGrounded = false;
            }

            // Looping inside the LevelEditor
            LevelEditor.main.Update();

            // Updating the player collisions
            player.Update(map.GetTiles(), map.GetDoors());

            Camera.main.UpdateCamera(GraphicsDevice.Viewport);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            // Background color
            GraphicsDevice.Clear(Color.DarkGray);

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, Camera.main.Transform);
            uiBatch.Begin();

            if (LevelEditor.enabled)
            {
                foreach (Tile t in LevelEditor.main.grid)
                {
                    t.Draw(spriteBatch);
                }
            }

            // Drawing the map
            map.DrawMap(spriteBatch);

            // Drawing the player
            player.Draw(spriteBatch);

            // Drawing the tile that follows the cursor
            if (LevelEditor.enabled)
            {
                LevelEditor.main.mouse.Draw(spriteBatch);

                // Debug left text
                uiBatch.DrawString(debugFont, "POS\n    X: " + Math.Round(Camera.main.Position.X, 2) + "\n    Y: " + Math.Round(Camera.main.Position.Y, 2)
                    + "\n\nVEL\n    X: " + Math.Round(player.GetSpeed().X, 2) + "\n    Y: " + Math.Round(player.GetSpeed().Y, 2), new Vector2(20, 10), Color.Red);

                uiBatch.DrawString(debugFont, "LEVEL EDITOR ENABLED", new Vector2(20, screenSize.Y - 40), Color.White);
            }

            spriteBatch.End();
            uiBatch.End();

            base.Draw(gameTime);
        }
    }
}
