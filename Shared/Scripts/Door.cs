﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Jitter
{
    internal class Door : Sprite
    {
        public static Texture2D doorSprite;
        public static Texture2D doorYellowSprite;
        public static Texture2D doorGreenSprite;
        public static Texture2D doorBlueSprite;
        public static Texture2D doorRedSprite;
        public static Texture2D doorOrangeSprite;

        public string levelToLoad = "";

        public enum Colors { Gray, Yellow, Green, Blue, Red, Orange, Delete, Construction };

        public Colors doorColor;

        public Door(Vector2 pos, Colors doorColor, string level)
        {
            this.doorColor = doorColor;
            levelToLoad = level;
            base.position = pos;

            UpdateColor();
        }

        public void UpdateColor()
        {
            if (this.doorColor == Colors.Gray)
            {
                base.sprite = Door.doorSprite;
            }
            else if (this.doorColor == Colors.Yellow)
            {
                base.sprite = Door.doorYellowSprite;
            }
            else if (this.doorColor == Colors.Green)
            {
                base.sprite = Door.doorGreenSprite;
            }
            else if (this.doorColor == Colors.Blue)
            {
                base.sprite = Door.doorBlueSprite;
            }
            else if (this.doorColor == Colors.Red)
            {
                base.sprite = Door.doorRedSprite;
            }
            else if (this.doorColor == Colors.Orange)
            {
                base.sprite = Door.doorOrangeSprite;
            }
            else if (this.doorColor == Colors.Construction)
            {
                //base.sprite = Tile.constructionTileSprite;
            }
            else if (this.doorColor == Colors.Delete)
            {
                //base.sprite = Tile.deleteTileSprite;
            }
        }

        public override string ToString()
        {
            return "D|" + position.X + "|" + position.Y + "|" + (int)doorColor + "|" + levelToLoad;
        }
    }
}
