﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Jitter
{
    internal class Tile : Sprite
    {
        public static Texture2D tileSprite;
        public static Texture2D tileYellowSprite;
        public static Texture2D tileGreenSprite;
        public static Texture2D tileBlueSprite;
        public static Texture2D tileRedSprite;
        public static Texture2D tileOrangeSprite;

        public static Texture2D constructionTileSprite;
        public static Texture2D deleteTileSprite;

        public enum Type { Gray, Yellow, Green, Blue, Red, Orange, Delete, Construction };

        public Type tileColor;

        public void UpdateColor()
        {
            if (tileColor == Type.Gray)
            {
                base.sprite = Tile.tileSprite;
            }
            else if (tileColor == Type.Yellow)
            {
                base.sprite = Tile.tileYellowSprite;
            }
            else if (tileColor == Type.Green)
            {
                base.sprite = Tile.tileGreenSprite;
            }
            else if (tileColor == Type.Blue)
            {
                base.sprite = Tile.tileBlueSprite;
            }
            else if (tileColor == Type.Red)
            {
                base.sprite = Tile.tileRedSprite;
            }
            else if (tileColor == Type.Orange)
            {
                base.sprite = Tile.tileOrangeSprite;
            }
            else if (tileColor == Type.Construction)
            {
                base.sprite = Tile.constructionTileSprite;
            }
            else if (tileColor == Type.Delete)
            {
                base.sprite = Tile.deleteTileSprite;
            }
        }

        public Tile(Vector2 pos, Type tileColor)
        {
            this.tileColor = tileColor;

            base.position = pos;

            UpdateColor();
        }

        public override string ToString()
        {
            return position.X + "|" + position.Y + "|" + (int)tileColor;
        }
    }
}
